﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jp.Co.Newral.CSharpMVP
{
    public partial class MainForm : Form, IMainForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public string title
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public string author
        {
            get { return label2.Text; }
            set { label2.Text = value; }
        }

        public Action<string> ShowMessageCommand { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowMessageCommand("button1 clicked.");
        }
    }
}
