﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jp.Co.Newral.CSharpMVP
{
    class MainFormPresenter
    {
        private IMainForm view;

        public MainFormPresenter(IMainForm view)
        {
            this.view = view;
            Initialize();
        }

        private void Initialize()
        {
            view.title = "WindowsForm Test";
            view.author = "Newral";
            view.ShowMessageCommand += (msg) =>
            {
                MessageBox.Show(msg);
            };
        }
    }
}
