﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jp.Co.Newral.CSharpMVP
{
    interface IMainForm
    {
        string title { get; set; }
        string author { get; set; }
        Action<string> ShowMessageCommand { get; set; }
    }
}
